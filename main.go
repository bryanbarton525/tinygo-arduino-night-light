package main

/* TinyGo
 * Use lazy concurrency for independent blinking LEDs
 * Target is Seeeduino XIAO
 * Bob U. 2021/08/01
 */

import (
	"time"

	"github.com/tinygo-org/tinygo/src/machine"
)

// declare and init LED pins outside of main()
var led1 = machine.D5 // white LED
var led2 = machine.D6 // orange LED

func main() {
	// config pins
	led1.Configure(machine.PinConfig{Mode: machine.PinOutput})
	led2.Configure(machine.PinConfig{Mode: machine.PinOutput})

	// run slow_blink as concurrent goroutine
	go slow_blink()

	// fast_blink runs in main() forever
	for {
		led1.Low()
		time.Sleep(time.Millisecond * 199)

		led1.High()
		time.Sleep(time.Millisecond * 67)
	}
}

func slow_blink() {
	// blink without end
	for {
		led2.Low()
		time.Sleep(time.Millisecond * 987)

		led2.High()
		time.Sleep(time.Millisecond * 1234)
	}
}
